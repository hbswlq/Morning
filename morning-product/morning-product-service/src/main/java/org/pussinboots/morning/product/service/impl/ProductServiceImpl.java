package org.pussinboots.morning.product.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import org.apache.commons.collections.CollectionUtils;
import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.product.entity.Category;
import org.pussinboots.morning.product.entity.Product;
import org.pussinboots.morning.product.entity.ProductCategory;
import org.pussinboots.morning.product.mapper.CategoryMapper;
import org.pussinboots.morning.product.mapper.ProductCategoryMapper;
import org.pussinboots.morning.product.mapper.ProductMapper;
import org.pussinboots.morning.product.pojo.vo.ProductVO;
import org.pussinboots.morning.product.service.IProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 项目名称：morning-product-service
 * 类名称：ProductServiceImpl
 * 类描述：Product / 商品表 业务逻辑层接口实现
 * 创建人：Damon
 * 创建时间：2017年4月11日 下午3:17:31
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public ProductVO getByNumber(Long productNumber, Integer status) {
        return productMapper.getByNumber(productNumber, status);
    }

    @Override
    public BasePageDTO<Product> listByPage(PageInfo pageInfo, String search) throws Exception {
        Page<Product> page = new Page<>(pageInfo.getCurrent(), pageInfo.getLimit());
        List<Product> productList = productMapper.selectListByPage(search, pageInfo, page);
        pageInfo.setTotal(page.getTotal());

        return new BasePageDTO<>(pageInfo, productList);
    }

    @Override
    public Integer updateProduct(Product product, Long categoryId) {
        ProductCategory queryProductCategory = new ProductCategory();
        queryProductCategory.setProductId(product.getProductId());
        ProductCategory productCategory = productCategoryMapper.selectOne(queryProductCategory);

        if (categoryId != null && !categoryId.equals(productCategory.getCategoryId())) {
            ProductCategory updateProductCategory = new ProductCategory();
            BeanUtils.copyProperties(queryProductCategory,updateProductCategory);
            updateProductCategory.setCategoryId(categoryId);
            productCategoryMapper.updateById(updateProductCategory);
        }

        return productMapper.updateById(product);
    }

    @Override
    public boolean insertProductInfo(String userName, Product product, Long categoryId) {
        int res = productMapper.insert(product);

        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductId(product.getProductId());
        productCategory.setCategoryId(categoryId);
        productCategory.setCreateTime(new Date());
        productCategory.setCreateBy(userName);
        productCategoryMapper.insert(productCategory);
        return res > 0;
    }

}
