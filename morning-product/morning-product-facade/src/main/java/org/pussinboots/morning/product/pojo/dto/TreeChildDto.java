package org.pussinboots.morning.product.pojo.dto;

import java.io.Serializable;

public class TreeChildDto implements Serializable {

    private static final long serialVersionUID = -6257562319078993714L;
    private Long id;
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
