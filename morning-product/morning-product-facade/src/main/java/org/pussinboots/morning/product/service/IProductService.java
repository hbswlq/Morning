package org.pussinboots.morning.product.service;

import org.pussinboots.morning.common.base.BasePageDTO;
import org.pussinboots.morning.common.support.page.PageInfo;
import org.pussinboots.morning.product.entity.Category;
import org.pussinboots.morning.product.entity.Product;
import org.pussinboots.morning.product.pojo.vo.ProductVO;

import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * 
* 项目名称：morning-product-facade   
* 类名称：IProductService   
* 类描述：Product / 商品表 业务逻辑层接口          
* 创建人：Damon
* 创建时间：2017年4月11日 下午3:13:06   
*
 */
public interface IProductService extends IService<Product> {
	
	/**
	 * 根据商品编号和状态查找商品
	 * @param productNumber 商品编号
	 * @param status 状态
	 * @return
	 */
	ProductVO getByNumber(Long productNumber, Integer status);

	/**
	 * 产品列表
	 * @param pageInfo
	 * @param search
	 * @return
	 */
	BasePageDTO<Product> listByPage(PageInfo pageInfo, String search) throws Exception;

	/**
	 * 更新产品
	 * @param product
	 * @return
	 */
	Integer updateProduct(Product product, Long categoryId);

	/**
	 * 添加产品
	 *
	 * @param userName
	 * @param product
	 * @param categoryIds
	 * @return
	 */
	boolean insertProductInfo(String userName, Product product, Long categoryIds);
}
