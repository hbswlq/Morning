package org.pussinboots.morning.product.pojo.dto;

import java.io.Serializable;
import java.util.List;

public class CategoryTreeDto implements Serializable {


    private static final long serialVersionUID = 156230192757915190L;
    private Long id;
    private String text;
    private State state = new State();
    private List<CategoryTreeDto> nodes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<CategoryTreeDto> getNodes() {
        return nodes;
    }

    public void setNodes(List<CategoryTreeDto> nodes) {
        this.nodes = nodes;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public class State{
        private Boolean selected;

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }


}
