package org.pussinboots.morning.cms.common.exception;

import java.nio.file.AccessDeniedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * 全局捕获异常
 * @author MyCp
 */
@ControllerAdvice(basePackages = "org.pussinboots.morning.cms.controller")
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public void runtimeException(Exception e) {
		e.printStackTrace();
	}

}
