<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%@ include file="/WEB-INF/layouts/base.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>创建商品 - 物通Morning</title>
    <link href="${ctxsta}/common/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="${ctxsta}/common/icheck/flat/green.css"/>
    <link rel="stylesheet" href="${ctxsta}/common/switchery/switchery.min.css"/>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>创建商品<small> 商品信息时应当遵循合法、正当、必要的原则，明示目的、方式和范围。</small></h5>
                    <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> <a
                            class="close-link"><i class="fa fa-times"></i></a></div>
                </div>
                <div class="ibox-content">
                    <form id="form" class="form-horizontal" action="${ctx}/product/create" data-method="post">
                        <div class="form-group m-t">
                            <label class="col-sm-2 col-xs-offset-1 control-label">商品分类：</label>
                            <div class="col-sm-7">
                                <input type="text" id="categoryName" name="categoryName" class="form-control"
                                       value=""
                                       placeholder="分类名称" style="width:200px;display:inline;" readonly="readonly">
                                <input type="hidden" id="categoryId" name="categoryId" value="">
                                <button class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#myModal" style="display:inline;">
                                    商品分类
                                </button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group m-t">
                            <label class="col-sm-2 col-xs-offset-1 control-label">商品名称：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group m-t">
                            <label class="col-sm-2 col-xs-offset-1 control-label">显示积分：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="showScore">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group m-t">
                            <label class="col-sm-2 col-xs-offset-1 control-label">显示价格：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="showPrice">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group m-t">
                            <label class="col-sm-2 col-xs-offset-1 control-label">商品简介：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="introduce">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-offset-1 control-label">展示图片：</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="picImg">
                            </div>
                            <a class="btn btn-info view-button"> <i class="fa fa-image"> </i> 查看 </a>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 col-xs-offset-3">
                                <input type="file" class="form-control">
                            </div>
                            <button class="btn btn-success upload-button" type="button"><i class="fa fa-upload"></i>&nbsp;&nbsp;<span
                                    class="bold">上传</span></button>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-offset-1 control-label">设置：</label>
                            <div class="col-sm-9">
                                <label class="radio-inline">
                                    <input type="checkbox" class="js-switch" name="showInNav" value="1"/>
                                    是否导航</label>
                                <label class="radio-inline">
                                    <input type="checkbox" class="js-switch" name="showInTop" value="1"/>
                                    是否置顶</label>
                                <label class="radio-inline">
                                    <input type="checkbox" class="js-switch" name="showInHot" value="1"/>
                                    是否热门</label>
                                <label class="radio-inline">
                                    <input type="checkbox" class="js-switch" name="showInShelve" value="1"/>
                                    是否上架</label>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-primary" type="submit">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- 模态框（Modal） -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        商品分类
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="tree"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">取消
                    </button>
                    <button type="button" class="btn btn-primary" id="btn" data-dismiss="modal">
                        确定
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>


</div>
<myfooter>
    <script src="${ctxsta}/common/switchery/switchery.min.js"></script>
    <script src="${ctxsta}/common/bootstrap/js/bootstrap-treeview.js"></script>
    <!-- 自定义js -->
    <script src="${ctxsta}/cms/js/product.js"></script>
    <script src="${ctxsta}/cms/js/productCreate.js"></script>
</myfooter>
</body>
</html>
