$(function () {

    function getTree() {
        let categoryId = $("#categoryId").val();
        let data = [];
        console.log("开始执行请求")
        $.ajax({
            url: baselocation + "/product/category/tree/list",
            data: {"categoryId": categoryId},
            type: "get",
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            async: false,
            success: function (res) {
                if (res.code == 1) {
                    data.push(res.data);
                } else {
                    layer.msg(res.message, {
                        icon : 2,
                        time : 1000
                    });
                }

            },
            error: function (res) {
                console.log(JSON.stringify(res))
            }
        })
        return data;
    }

    $('#tree').treeview({
        data: getTree(),     // data is not optional
        levels: 2,
        multiSelect: false,

    });

    $("#btn").click(function (e) {

        var arr = $('#tree').treeview('getSelected');
        var selectedDataName = [];
        var selectedData = [];
        for (var key in arr) {
            selectedDataName.push(arr[key].text);
            selectedData.push(arr[key].id);
        }
        $("#categoryName").val(selectedDataName)
        $("#categoryId").val(selectedData)
    })


})